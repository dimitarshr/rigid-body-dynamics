#pragma once
#include "Body.h"

class RigidBody : public Body {
public:
	RigidBody();
	~RigidBody();

	//set adnd get methods
	void setAngVel(const glm::vec3 &omega) { m_angVel = omega; }
	void setAngAccl(const glm::vec3 &alpha) { m_angAcc = alpha; }

	glm::vec3 getAngVel() { return m_angVel; }
	glm::vec3 getAngAcc() { return m_angAcc; }
	glm::mat3 getInvInertia();
	void scale(glm::vec3 vect);
	void setMass(float mass);

private:
	float m_density;
	glm::mat3 m_invInertia;
	glm::vec3 m_angVel;
	glm::vec3 m_angAcc;
	void setInvInertia();
};