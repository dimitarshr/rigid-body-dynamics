#include <iostream>
#include <cmath>
#include "Force.h"
#include "Body.h"
#include "glm/ext.hpp"

glm::vec3 Force::apply(float mass, const glm::vec3 &pos, const glm::vec3 &vel) {
	return glm::vec3(0.0f);
}

/*
** GRAVITY
*/
glm::vec3 Gravity::apply(float mass, const glm::vec3 &pos, const glm::vec3 &vel) {
	return this->getGravity() * mass;
}

/*
** DRAG
*/
glm::vec3 Drag::apply(float mass, const glm::vec3 &pos, const glm::vec3 &vel) {
	if (glm::length(vel) == 0.0f) {
		return glm::vec3(0.0f);
	}
	return 0.5f * this->getAirDensity() * (vel * vel) * this->getDragCoeff() * this->getCrossSection() * (-1.0f) * glm::normalize(vel);
}

/*
** HOOKE'S LAW
*/
glm::vec3 Hooke::apply(float mass, const glm::vec3 &pos, const glm::vec3 &vel) {
	float distance = glm::length(this->getB2()->getPos() - this->getB1()->getPos());
	glm::vec3 e = glm::vec3(0.0f);

	if (distance != 0) {
		e = (this->getB2()->getPos() - this->getB1()->getPos()) / distance;
	}

	float v1 = glm::dot(this->getB1()->getVel(), e);
	float v2 = glm::dot(this->getB2()->getVel(), e);

	float simpleSpringForce = (-1.0f) * this->getSpringStiffness() * (this->getRestLen() - distance);

	float dampingForce = (-1.0f) * this->getDampingCoeff() * (v1 - v2);
	
	return e * (simpleSpringForce + dampingForce);
}

/*
** Aerodynamic force
*/

glm::vec3 AerodynamicForce::apply(float mass, const glm::vec3 &pos, const glm::vec3 &vel) {
	glm::vec3 wind = this->getWindSpeed();
	glm::vec3 vSurface = (this->getB1()->getVel() + this->getB2()->getVel() + this->getB3()->getVel()) / 3.0f;

	if (glm::length(vSurface) == 0.0f) {
		return glm::vec3(0.0f);
	}

	vSurface = vSurface - wind;

	glm::vec3 crossProduct = glm::cross((this->getB2()->getPos() - this->getB1()->getPos()), (this->getB3()->getPos() - this->getB1()->getPos()));
	
	glm::vec3 n = crossProduct / glm::length(crossProduct);

	float area = 0.5f * glm::length(crossProduct);

	glm::vec3 velocityUnit = vSurface / glm::length(vSurface);
	float crossSectionArea = area * (glm::dot(velocityUnit, n));

	glm::vec3 fAero = 0.5f * 1.225f * glm::length(vSurface) * glm::length(vSurface) * 1.05 * crossSectionArea * n * -1.0f;
	return (fAero / 3.0f);

}