#pragma once
// Math constants
#define _USE_MATH_DEFINES
#include <cmath>  
#include <random>

// Std. Includes
#include <string>
#include <time.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/matrix_operation.hpp>
#include "glm/ext.hpp"

// Other Libs
#include "SOIL2/SOIL2.h"

// project includes
#include "Application.h"
#include "Shader.h"
#include "Mesh.h"
#include "Particle.h"
#include "RigidBody.h"


// time
GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

void rigidBodyCollisionDetection(RigidBody *rb, Mesh plane, bool friction) {
	std::vector<glm::vec3> pointsOfContacts;
	
	for (Vertex v : rb->getMesh().getVertices()) {
		glm::vec3 currentPosition = (rb->getMesh().getModel() * glm::vec4(v.getCoord(), 1.0));
		if (currentPosition[1] < plane.getPos()[1]) {
			pointsOfContacts.push_back(currentPosition);
		}
	}

	if (pointsOfContacts.size() > 0) {
		
		std::cout << "Collision detected at:" << std::endl;
		for each (glm::vec3 Vertex in pointsOfContacts)
		{
			std::cout <<"\t"<< glm::to_string(Vertex) << std::endl;
		}

		glm::vec3 contactPoint = glm::vec3(0.0f);
		glm::vec3 translationVector = glm::vec3(0.0f);
		translationVector[1] = pointsOfContacts[0][1];

		for (glm::vec3 v : pointsOfContacts) {
			if (v[1] < translationVector[1]) {
				translationVector[1] = v[1];
			}
			contactPoint += v;
		}

		translationVector[1] = std::abs(translationVector[1]);
		rb->translate(translationVector);

		contactPoint = contactPoint / pointsOfContacts.size();

		std::cout << "Average of all colliding coordinates:" << std::endl;
		std::cout << "\t" << glm::to_string(contactPoint) << std::endl;
		std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

		
		if (glm::length(rb->getVel()) != 0.0f || glm::length(rb->getAngVel()) != 0.0f) {
			float e = rb->getCor();
			glm::vec3 n = glm::normalize(glm::vec3(0.0f, 1.0f, 0.0f));
			glm::vec3 impulsePoint = contactPoint - rb->getPos();
			glm::vec3 vR = rb->getVel() + glm::cross(rb->getAngVel(), impulsePoint);

			float d = glm::dot(vR, n);

			float bottomPart = glm::dot(glm::cross(rb->getInvInertia() * glm::cross(impulsePoint, n), impulsePoint), n);
			float jR = (-1.0f*(1.0f + e) * d) / (1.0f / rb->getMass() + bottomPart);

			glm::vec3 impulse = jR * n;

			rb->setVel(rb->getVel() + impulse / rb->getMass());
			rb->setAngVel(rb->getAngVel() + rb->getInvInertia() * glm::cross(impulsePoint, impulse));

			/*Friction*/
			if (friction) {
				float mu = 0.09f;
				glm::vec3 vT = glm::vec3(0.0f);
				if (glm::length(vR - d * n) != 0.0f) {
					vT = glm::normalize(vR - d * n); 
				}
				glm::vec3 jT = glm::vec3(0.0f);
				if (glm::length(vT) != 0.0f) {
					jT = -1.0f *mu * abs(jR) * vT;
				}

				if (glm::length(rb->getVel()) <= 0.2f) {
					rb->setVel(glm::vec3(0.0f));
				}
				else {
					rb->setVel(rb->getVel() + jT / rb->getMass());
				}

				if (glm::length(rb->getAngVel()) <= 0.2f) {
					rb->setAngVel(glm::vec3(0.0f));
				}
				else {
					rb->setAngVel(rb->getAngVel() + rb->getInvInertia() * glm::cross(impulsePoint, jT));
				}
			}
			//*********************************************************************************************************
		}
	}
}
// main function
int main()
{
	// create application
	Application app = Application::Application();
	app.initRender();
	Application::camera.setCameraPosition(glm::vec3(0.0f, 5.0f, 20.0f));
			
	// create ground plane
	Mesh plane = Mesh::Mesh(Mesh::QUAD);
	// scale it up x5
	plane.scale(glm::vec3(5.0f, 5.0f, 5.0f));
	plane.setShader(Shader("resources/shaders/physics.vert", "resources/shaders/physics.frag"));

	// Create the cube objects
	Mesh cube = Mesh::Mesh("resources/models/cube.obj");
	cube.translate(glm::vec3(0.0f, 4.9f, 0.0f));
	cube.scale(glm::vec3(10.0f, 10.0f, 10.0f));
	cube.setShader(Shader("resources/shaders/physics.vert", "resources/shaders/solid_transparent.frag"));

	// time
	const double dt = 0.01;
	double startTime = (GLfloat) glfwGetTime();
	double accumulator = 0.0;

	/*******************************************************************/
	/*
	** Rigid Body
	** Task 1
	*/
	RigidBody *rbImpulsed = new RigidBody();
	Mesh mCube = Mesh::Mesh(Mesh::CUBE);
	rbImpulsed->setMesh(mCube);
	rbImpulsed->scale(glm::vec3(1.0f, 3.0f, 1.0f));
	rbImpulsed->getMesh().setShader(Shader("resources/shaders/physics.vert", "resources/shaders/physics.frag"));
	// Rigid body mothion values
	rbImpulsed->translate(glm::vec3(0.0f, 3.0f, 0.0f));
	rbImpulsed->setVel(glm::vec3(2.0f, 0.0f, 0.0f));
	rbImpulsed->setAngVel(glm::vec3(0.0f, 0.0f, 0.0f));
	rbImpulsed->setMass(2.0f);

	bool impulseApplied = false;
	GLfloat animationStart = (GLfloat)glfwGetTime();
	
	/*Printing inverse inertia*/
	std::cout << glm::to_string(rbImpulsed->getInvInertia()).c_str() << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	/*******************************************************************/

	/*
	** Rigid Body
	** Task 2
	*/
	RigidBody *rbCollision = new RigidBody();
	Mesh mCubeCol = Mesh::Mesh(Mesh::CUBE);
	rbCollision->setMesh(mCubeCol);
	rbCollision->scale(glm::vec3(1.0f, 3.0f, 1.0f));
	rbCollision->getMesh().setShader(Shader("resources/shaders/physics.vert", "resources/shaders/physics.frag"));
	// Rigid body mothion values
	rbCollision->setMass(2.0f);
	rbCollision->setCor(0.7f);

	Gravity *gravity = new Gravity();
	rbCollision->addForce(gravity);
	/*******************************************************************/

	/*
	** Taks 3
	*/
	RigidBody *rbWithFriction = new RigidBody();
	Mesh meshFriction = Mesh::Mesh(Mesh::CUBE);
	rbWithFriction->setMesh(meshFriction);
	rbWithFriction->scale(glm::vec3(1.0f, 3.0f, 1.0f));
	rbWithFriction->getMesh().setShader(Shader("resources/shaders/physics.vert", "resources/shaders/physics.frag"));
	// Rigid body mothion values
	rbWithFriction->setMass(2.0f);
	rbWithFriction->setCor(0.4f);

	rbWithFriction->addForce(gravity);
	/*******************************************************************/

	bool task1 = true;
	bool task2 = false;
	bool task3 = false;

	// Game loop
	while (!glfwWindowShouldClose(app.getWindow()))
	{
		if (app.keys[GLFW_KEY_1]) {
			task1 = true;
			task2 = false;
			task3 = false;

			rbImpulsed->setPos(glm::vec3(0.0f, 3.0f, 0.0f));
			rbImpulsed->setVel(glm::vec3(2.0f, 0.0f, 0.0f));
			rbImpulsed->setAngVel(glm::vec3(0.0f, 0.0f, 0.0f));
			rbImpulsed->setAcc(glm::vec3(0.0f, 0.0f, 0.0f));
			rbImpulsed->setRotate(glm::mat4(1.0f));
			animationStart = (GLfloat)glfwGetTime();
			
			impulseApplied = false;
		}
		else if (app.keys[GLFW_KEY_2]) {
			task1 = false;
			task2 = true;
			task3 = false;

			rbCollision->setPos(glm::vec3(0.0f, 12.0f, 0.0f));
			rbCollision->setVel(glm::vec3(0.0f, 0.0f, 0.0f));
			rbCollision->setAngVel(glm::vec3(0.1f, 0.1f, 0.1f));
			rbCollision->setRotate(glm::mat4(1.0f));
		}
		else if (app.keys[GLFW_KEY_3]) {
			task1 = false;
			task2 = false;
			task3 = true;

			rbWithFriction->setPos(glm::vec3(0.0f, 12.0f, 0.0f));
			rbWithFriction->setVel(glm::vec3(0.0f, 0.0f, 0.0f));
			rbWithFriction->setAngVel(glm::vec3(0.1f, 0.1f, 0.1f));
			rbWithFriction->setRotate(glm::mat4(1.0f));
		}
		/*
		**	SIMULATION
		*/
		GLfloat newTime = (GLfloat)glfwGetTime();
		GLfloat frameTime = newTime - startTime;
		startTime = newTime;
		accumulator += frameTime;

		if (task1) {
			while (accumulator >= dt) {
				app.doMovement(dt);
				
				if ((GLfloat)glfwGetTime() - animationStart >= 2.0f && !impulseApplied) {
					//impulse
					glm::vec3 impulse = glm::vec3(-4.0f, 0.0f, 0.0f);
					rbImpulsed->setVel(rbImpulsed->getVel() + impulse / rbImpulsed->getMass());

					//impulse application point
					glm::vec3 CoM = rbImpulsed->getPos();
					glm::vec3 impulsePoint = CoM + glm::vec3(1.5f, -2.0f, 0.0f);

					glm::vec3 omega = rbImpulsed->getInvInertia() * glm::cross(impulsePoint-CoM, impulse);
					rbImpulsed->setAngVel(rbImpulsed->getAngVel() + omega);

					impulseApplied = true;
				}

				rbImpulsed->translate(dt * rbImpulsed->getVel());
				
				glm::mat3 angVelSkew = glm::matrixCross3(rbImpulsed->getAngVel());
				glm::mat3 R = glm::mat3(rbImpulsed->getRotate());
				R += dt * angVelSkew * R;
				R = glm::orthonormalize(R);
				
				rbImpulsed->setRotate(glm::mat4(R));

				accumulator -= dt;
				std::cout << glm::to_string(rbImpulsed->getInvInertia()).c_str() << std::endl;
				std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
			}
		}
		else if (task2) {
			while (accumulator >= dt) {
				app.doMovement(dt);

				rbCollision->setAcc(rbCollision->applyForces(rbCollision->getPos(), rbCollision->getVel(), newTime, dt));
				rbCollision->setVel(rbCollision->getVel() + dt * rbCollision->getAcc());
				rbCollision->translate(dt * rbCollision->getVel());

				glm::mat3 angVelSkew = glm::matrixCross3(rbCollision->getAngVel());
				glm::mat3 R = glm::mat3(rbCollision->getRotate());
				R += dt * angVelSkew * R;
				R = glm::orthonormalize(R);
				rbCollision->setRotate(glm::mat4(R));
				rbCollision->setAngVel(rbCollision->getAngVel() + dt * rbCollision->getAngAcc());

				rigidBodyCollisionDetection(rbCollision, plane, false);

				accumulator -= dt;
			}
		}
		else if (task3) {
			while (accumulator >= dt) {
				app.doMovement(dt);

				rbWithFriction->setAcc(rbWithFriction->applyForces(rbWithFriction->getPos(), rbWithFriction->getVel(), newTime, dt));
				rbWithFriction->setVel(rbWithFriction->getVel() + dt * rbWithFriction->getAcc());
				rbWithFriction->translate(dt * rbWithFriction->getVel());

				glm::mat3 angVelSkew = glm::matrixCross3(rbWithFriction->getAngVel());
				glm::mat3 R = glm::mat3(rbWithFriction->getRotate());
				R += dt * angVelSkew * R;
				R = glm::orthonormalize(R);
				rbWithFriction->setRotate(glm::mat4(R));
				rbWithFriction->setAngVel(rbWithFriction->getAngVel() + dt * rbWithFriction->getAngAcc());

				rigidBodyCollisionDetection(rbWithFriction, plane, true);

				accumulator -= dt;
			}
		}


		/*
		**	RENDER 
		*/		
		// clear buffer
		app.clear();
		// draw groud plane
		app.draw(plane);

		// draw the rigid body
		if (task1) {
			app.draw(rbImpulsed->getMesh());
		}
		else if (task2) {
			app.draw(rbCollision->getMesh());
		}
		else if (task3) {
			app.draw(rbWithFriction->getMesh());
		}

		// Draw the cube objects
		app.draw(cube);

		app.display();
	}

	app.terminate();

	return EXIT_SUCCESS;
}

